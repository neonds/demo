
/*
 *  Modulo javascript que maneja el formulario del login
 *	autor: gdiaz
 *  since 1.0
 *  date: 13/10/16
 */
var LoginController =  (function (smartcardService, jquery) {



	var button = jquery("#click");



	//Events
	button.click(function (e) {
		var loginModel = jquery("#login-form").serializeJSON();

		smartcardService.getCertificates(loginModel, function (data) {
			
			if (data == 403){
				alert("Pin incorrecto");
			}
			if (data == 404){
				alert("Lectora o Tarjeta desconectada");
			}

			if (data == 0){
				alert("El servicio de la lectora no está en ejecución")
			}

			if(isNaN(data) ){
				jquery("#login-form").hide();
				jquery("#result").show();
				jquery.each(data, function (i, object) {
					var name = object.name;
					var alias = object.alias;
					var cert = object.encodedCertificate;
					jquery("#certs").append("<li><a href='#'>" + name + "</a></li>");
				
				});
			}



		});

	});



	_public = {

	};


	return _public;



}); // Se le pasa por parametro el servicio que conecta con SmartCard y jquery

//Boots app
$(document).ready(function () {
	LoginController(SmartcardService, $);
});