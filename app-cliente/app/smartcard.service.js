
/*
 *  Modulo javascript que permite el acceso al servicio local.
 *	autor: gdiaz
 *  since 1.0
 *  date: 13/10/16
 */
var SmartcardService =  (function (jquery) {
	

	//private 
	var _API = "http://localhost:4567/smartcard/certificates";




	_public = {

		//Hace una solicitud a el smartcard por el certificado
		// envía el pin
		getCertificates: function  (loginModel, callback) {

			$.ajax({
			  type: 'POST',
			  url: _API,
			  data: { 'pin': loginModel.pin } ,
			  success: function  (data, header) {
			  	if (data){
			  		callback(data);	
			  	}

			  },
			  error: function  (xhr, ajaxOptions, thrownError) {
			  		callback(xhr.status);
			  }
			});

		},


		//metodo para validar el servicio
		sayHello: function  () {
			alert("Hello");
		}



	};


	return _public;



})($);
